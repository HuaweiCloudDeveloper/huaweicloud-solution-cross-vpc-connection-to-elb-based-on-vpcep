[TOC]

**解决方案介绍**
===============
该解决方案基于终端节点和终端节点服务，帮助用户快速实现同一区域不经过公网跨VPC的ELB后端服务访问。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/cross-vpc-connection-to-elb-based-on-vpcep.html](https://www.huaweicloud.com/solution/implementations/cross-vpc-connection-to-elb-based-on-vpcep.html)

**架构图**
---------------

![方案架构](./document/cross-vpc-connection-to-elb-based-on-vpcep.png)

**架构描述**
---------------

该解决方案会部署如下资源：

1、创建弹性云服务器 ECS，用于访问和提供业务服务。

2、创建安全组，通过配置安全组规则，为弹性云服务器提供安全防护。

3、创建一个终端节点 VPCEP，用于私密连接终端节点服务。

4、创建一个终端节点服务，用于将云服务或用户私有服务配置为VPC终端节点支持的服务，可以被终端节点连接和访问。

5、创建弹性负载均衡 ELB，为终端节点服务提供业务保障。


**组织结构**
---------------

``` lua
huaweicloud-cross-vpc-connection-to-elb-based-on-vpcep
├── cross-vpc-connection-to-elb-based-on-vpcep.json -- 资源编排模板
```
**开始使用**
---------------

1、查看虚拟私有云VPC实例

在[虚拟私有云VPC控制台](https://console.huaweicloud.com/vpc/?region=cn-north-4&locale=zh-cn#/vpc/vpcs/list)，可查看该方案一键生成的VPC和对应的子网/路由表/弹性服务器ECS。

图1 VPC实例
![VPC实例](./document/readme-image-001.PNG)

2、查看终端节点服务实例

在[VPC终端节点服务控制台](https://console.huaweicloud.com/vpc/?agencyId=21c19ac150bf4867a8302133acfa94ec&region=cn-north-4&locale=zh-cn#/ep/service)，查看该方案一键部署创建的弹性公网IP实例。

图2 终端节点服务实例
![终端节点服务实例](./document/readme-image-002.PNG)


3、查看终端节点实例

在[VPC终端节点控制台](https://console.huaweicloud.com/vpc/?agencyId=21c19ac150bf4867a8302133acfa94ec&region=cn-north-4&locale=zh-cn#/ep/list)，查看该方案一键部署创建的公网NAT网关实例和添加的SNAT规则。

图3 终端节点实例
![终端节点实例](./document/readme-image-003.PNG)


4、验证VPC1中的弹性云服务器， 私密访问VPC终端节点，测试网络连通性

在[弹性云服务器控制台](https://console.huaweicloud.com/ecm/?region=cn-north-4#/ecs/manager/vmList)，可远程登录云服务器，通过ping IP的方式测试与终端节点的连通性

图5 远程登录服务器ECS
![测试与终端节点连通性](./document/readme-image-004.PNG)


